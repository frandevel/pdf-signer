package main;

import es.devel.pdfsigner.es.devel.pdfsigner.model.SignatureType;
import es.devel.pdfsigner.service.SignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final String SIGNED_SUFFIX = "_signed";

    public static void main(String[] args) throws Exception {
        String reason = "Visado";
        String location = "Valencia";
        boolean deleteFilesAfterSigning = true;
        String certificateFileName = "firma.p12";

        if(args.length < 3 || args.length > 5) {
            logger.error("Bad arguments provided. \nUsage: java -jar pdf-signer.jar /Users/user/pdf-container passw0rd firma.p12 [Visado Valencia]");
            logger.info("All PDF files in the current folder will be signed and renamed to <filename>_signed.pdf\n\n");
            System.exit(1);
        } else {
            if(args.length == 3) {
                certificateFileName = args[2];
            }
            if(args.length == 4) {
                certificateFileName = args[2];
                reason = args[3];
            }
            if (args.length == 5) {
                certificateFileName = args[2];
                reason = args[3];
                location = args[4];
            }
        }

        String containingFolder = args[0];
        logger.info("Signing PDF files in " + containingFolder);

        String password = args[1];

        int signedFilesCount = SignService.signDocuments(containingFolder, password, new File(certificateFileName), SignatureType.DEFAULT, reason, location, deleteFilesAfterSigning, SIGNED_SUFFIX);
        logger.info("Total of " + signedFilesCount + " were signed");
    }

}
