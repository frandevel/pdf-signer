package es.devel.pdfsigner.es.devel.pdfsigner.model;

public enum SignatureType {
    DEFAULT("PKCS12");

    private String type;

    private SignatureType(String type) {
        this.type = type;
    }

    public String type() {
        return type;
    }

}
