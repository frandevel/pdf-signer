package es.devel.pdfsigner.service;

import es.devel.pdfsigner.es.devel.pdfsigner.model.SignatureType;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SignServiceTest {

    @Test
    @Ignore
    public void signDocuments_executionTest_documentsAreSigned() throws Exception {
        // Arrange
        String currentDir = System.getProperty("user.dir");

        // Act
        int signedCount = SignService.signDocuments(currentDir + "/src/test/resources", "myPassword", new File(currentDir + "/MyCertFile.p12"), SignatureType.DEFAULT, "My Reason", "MyLocation", false, "_signed");

        // Assert
        assertThat(signedCount, is(1));
    }

}
