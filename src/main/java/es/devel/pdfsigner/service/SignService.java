package es.devel.pdfsigner.service;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSignatureAppearance;
import com.lowagie.text.pdf.PdfStamper;
import es.devel.pdfsigner.es.devel.pdfsigner.model.SignatureType;
import es.devel.pdfsigner.filter.PdfFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;

public class SignService {

    private static final Logger logger = LoggerFactory.getLogger(SignService.class);
    private static final int PDF_EXTENSION_LENGTH = 4;

    public static int signDocuments(String folderContainingFilesToSign,
                                    String signPassword,
                                    File certificateFile,
                                    SignatureType signatureType,
                                    String reason,
                                    String location,
                                    boolean deleteFileAfterSigning, String signedSuffix) {

        File container = new File(folderContainingFilesToSign);
        if (container.isFile()) {
            throw new IllegalArgumentException("It's a file, not a folder. Please provide a folder that contains the files to be signed");
        }
        if (!container.exists() || !container.canWrite()) {
            throw new IllegalStateException("Folder is not writable. It does not exist or you don't have the needed permissions");
        }

        int count = 0;

        File[] files = container.listFiles(new PdfFileFilter());
        if (files.length == 0) {
            logger.info("No files found to be signed found in the specified folder: " + container.getAbsolutePath());
            System.exit(1);
        }
        for (File file : files) {
            if (file.canWrite() && file.isFile()) {
                logger.info("Signing file " + file.getName());
                try {
                    signPdfDocument(file, signPassword, certificateFile, signatureType, reason, location, signedSuffix);
                } catch (IOException e) {
                    throw new IllegalStateException("Could not find file: " + file.getAbsolutePath());
                }
                if (deleteFileAfterSigning) {
                    file.deleteOnExit();
                }
                count++;
            }
        }

        logger.info("All documents have been signed and are available under the same name with \"_signed\" appended");

        return count;
    }

    public static void signPdfDocument(File fileToBeSigned,
                                       String password,
                                       File certificateFile,
                                       SignatureType signatureType,
                                       String reason,
                                       String location, String signedSuffix) throws IOException {

        if (fileToBeSigned == null || fileToBeSigned.length() <= 0) {
            throw new IllegalArgumentException("File does not exist or is an invalid file");
        }

        PrivateKey key;
        java.security.cert.Certificate[] chain;
        PdfStamper stp;
        KeyStore ks;
        try {
            ks = KeyStore.getInstance(signatureType.type());
        } catch (KeyStoreException e) {
            throw new IllegalStateException("Invalid keystore: " + e);
        }
        try {
            ks.load(new FileInputStream(certificateFile), password.toCharArray());


            String alias;
            alias = ks.aliases().nextElement();

            key = (PrivateKey) ks.getKey(alias, password.toCharArray());
            chain = ks.getCertificateChain(alias);
        } catch (IOException e) {
            throw new IllegalStateException("Could not find file: " + certificateFile);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Signature type not supported: " + signatureType.type());
        } catch (CertificateException | UnrecoverableKeyException | KeyStoreException e) {
            throw new IllegalStateException("Invalid certificate file: " + certificateFile);
        }

        PdfReader reader = new PdfReader(new FileInputStream(fileToBeSigned));
        String filename = fileToBeSigned.getName();
        String filenameWithoutExtension = filename.substring(0, filename.length() - PDF_EXTENSION_LENGTH);
        FileOutputStream fout = new FileOutputStream(filenameWithoutExtension + signedSuffix + ".pdf");
        try {
            stp = PdfStamper.createSignature(reader, fout, '\0');
        } catch (DocumentException e) {
            throw new IllegalStateException("Could not sign the document: " + filename, e);
        }

        PdfSignatureAppearance sap = stp.getSignatureAppearance();
        sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
        sap.setReason(reason);
        sap.setLocation(location);
        try {
            stp.close();
        } catch (Exception e) {
            throw new IllegalStateException("Problems while trying to close the after after signing: ", e);
        }
    }

}


