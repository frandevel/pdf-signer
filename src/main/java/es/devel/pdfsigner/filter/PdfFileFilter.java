package es.devel.pdfsigner.filter;

import java.io.File;
import java.io.FilenameFilter;

public class PdfFileFilter implements FilenameFilter {

    public PdfFileFilter() {
    }

    @Override
    public boolean accept(File dir, String name) {
        if (name.endsWith("pdf") || name.endsWith("PDF")) {
            return true;
        }

        return false;
    }

}
